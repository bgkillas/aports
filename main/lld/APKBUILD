# Contributor: Eric Molitor <eric@molitor.org>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=lld
pkgver=15.0.5
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="The LLVM Linker"
url="https://llvm.org/"
# cannot link anything and fails
# ld.lld: error: unknown emulation: elf64_s390
# ld.lld: error: src/gn/gn_main.o: could not infer e_machine
# from bitcode target triple s390x-alpine-linux-musl
# also fails hundreds of tests
arch="all !s390x"
license="Apache-2.0"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	clang
	cmake
	libedit-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	llvm$_llvmver-test-utils
	llvm-libunwind-dev
	samurai
	zlib-dev
	"
checkdepends="gtest-dev bash llvm$_llvmver-test-utils"
subpackages="$pkgname-dbg $pkgname-libs $pkgname-dev $pkgname-doc"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/lld-$pkgver.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/cmake-$pkgver.src.tar.xz
	"
builddir="$srcdir/$pkgname-$pkgver.src"

case "$CARCH" in
armhf)
	# for some reason they hang forever, but the actual linker works fine
	options="$options !check"
	;;
esac

build() {
	CFLAGS="${CFLAGS/-g/-g1}" \
	CXXFLAGS="${CXXFLAGS/-g/-g1}" \
	CC=clang CXX=clang++ \
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_MODULE_PATH="$srcdir/cmake-$pkgver.src/Modules" \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DBUILD_SHARED_LIBS=ON \
		-DLLVM_INCLUDE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DLLVM_EXTERNAL_LIT=/usr/bin/lit \
		-DLLD_BUILT_STANDALONE=ON
	cmake --build build
}

check() {
	ninja -C build check-lld
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -Dm644 "$builddir"/docs/ld.lld.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
9f579e315a271f757da6d8c77409dd20197597b05394561a54261882220b734cf9fa3a3c883ce15d468822e5087c20059e9d2b0922959fd55f995de6875c6db9  lld-15.0.5.src.tar.xz
493825f1b64abef6d56502d28e30056320bdda5a46a478fc85eec3a631c541da2d318fb9c3e3d3e6234f538121b7a1c60d34cba498fd6b7938d3558d24684fed  cmake-15.0.5.src.tar.xz
"
