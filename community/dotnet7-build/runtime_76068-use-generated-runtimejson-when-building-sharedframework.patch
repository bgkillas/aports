From cd2f0ffb287251c26f03850d1a596abafb99d5c7 Mon Sep 17 00:00:00 2001
PatchSource: https://github.com/dotnet/runtime/pull/76068
From: Tom Deseyn <tom.deseyn@gmail.com>
Date: Tue, 27 Sep 2022 13:22:50 +0200
Subject: [PATCH] Use generated runtime.json when building shared framework
 packages. (#76068)

* Use generated runtime.json when building shared framework packages.

* Don't UpdateRuntimeJson on Build.

* PR feedback.

* Update Microsoft.NETCore.Platforms.csproj

Co-authored-by: Viktor Hofer <viktor.hofer@microsoft.com>
---
 Directory.Build.props                                      | 1 -
 eng/liveBuilds.targets                                     | 4 +++-
 .../src/Microsoft.NETCore.Platforms.csproj                 | 7 +++----
 src/libraries/oob-all.proj                                 | 3 +--
 src/libraries/oob-src.proj                                 | 3 +--
 src/libraries/pretest.proj                                 | 2 +-
 6 files changed, 9 insertions(+), 11 deletions(-)

diff --git a/src/runtime/Directory.Build.props b/src/runtime/Directory.Build.props
index 6bdf6ba2f204f..3e76022487983 100644
--- a/src/runtime/Directory.Build.props
+++ b/src/runtime/Directory.Build.props
@@ -274,7 +274,6 @@
     <PackageProjectUrl>https://dot.net</PackageProjectUrl>
     <Owners>microsoft,dotnetframework</Owners>
     <IncludeSymbols>true</IncludeSymbols>
-    <RuntimeIdGraphDefinitionFile>$([MSBuild]::NormalizePath('$(LibrariesProjectRoot)', 'Microsoft.NETCore.Platforms', 'src', 'runtime.json'))</RuntimeIdGraphDefinitionFile>
     <LicenseFile>$(MSBuildThisFileDirectory)LICENSE.TXT</LicenseFile>
     <PackageLicenseExpression>MIT</PackageLicenseExpression>
     <PackageRequireLicenseAcceptance>false</PackageRequireLicenseAcceptance>
diff --git a/src/runtime/eng/liveBuilds.targets b/src/runtime/eng/liveBuilds.targets
index 982413a4ebae2..3006fd92e756e 100644
--- a/src/runtime/eng/liveBuilds.targets
+++ b/src/runtime/eng/liveBuilds.targets
@@ -223,6 +223,8 @@
             ResolveLibrariesRuntimeFilesFromLocalBuild" />
 
   <PropertyGroup>
-    <BundledRuntimeIdentifierGraphFile>$(RuntimeIdGraphDefinitionFile)</BundledRuntimeIdentifierGraphFile>
+    <!-- Keep in sync with outputs defined in Microsoft.NETCore.Platforms.csproj. -->
+    <BundledRuntimeIdentifierGraphFile>$([MSBuild]::NormalizePath('$(ArtifactsBinDir)', 'Microsoft.NETCore.Platforms', 'runtime.json'))</BundledRuntimeIdentifierGraphFile>
+    <BundledRuntimeIdentifierGraphFile Condition="!Exists('$(BundledRuntimeIdentifierGraphFile)')">$([MSBuild]::NormalizePath('$(LibrariesProjectRoot)', 'Microsoft.NETCore.Platforms', 'src', 'runtime.json'))</BundledRuntimeIdentifierGraphFile>
   </PropertyGroup>
 </Project>
diff --git a/src/runtime/src/libraries/Microsoft.NETCore.Platforms/src/Microsoft.NETCore.Platforms.csproj b/src/runtime/src/libraries/Microsoft.NETCore.Platforms/src/Microsoft.NETCore.Platforms.csproj
index 482e0b70e47e7..742f17881891c 100644
--- a/src/runtime/src/libraries/Microsoft.NETCore.Platforms/src/Microsoft.NETCore.Platforms.csproj
+++ b/src/runtime/src/libraries/Microsoft.NETCore.Platforms/src/Microsoft.NETCore.Platforms.csproj
@@ -17,7 +17,6 @@
     <AvoidRestoreCycleOnSelfReference>true</AvoidRestoreCycleOnSelfReference>
     <!-- TODO: Remove with AvoidRestoreCycleOnSelfReference hack. -->
     <PackageValidationBaselineName>$(MSBuildProjectName)</PackageValidationBaselineName>
-    <BeforePack>GenerateRuntimeJson;UpdateRuntimeJson;$(BeforePack)</BeforePack>
     
     <_generateRuntimeGraphTargetFramework Condition="'$(MSBuildRuntimeType)' == 'core'">$(NetCoreAppToolCurrent)</_generateRuntimeGraphTargetFramework>
     <_generateRuntimeGraphTargetFramework Condition="'$(MSBuildRuntimeType)' != 'core'">net472</_generateRuntimeGraphTargetFramework>
@@ -44,7 +43,7 @@
 
   <ItemGroup>
     <Content Condition="'$(AdditionalRuntimeIdentifiers)' == ''" Include="runtime.json" PackagePath="/" />
-    <Content Condition="'$(AdditionalRuntimeIdentifiers)' != ''" Include="$(IntermediateOutputPath)runtime.json" PackagePath="/" />
+    <Content Condition="'$(AdditionalRuntimeIdentifiers)' != ''" Include="$(BaseOutputPath)runtime.json" PackagePath="/" />
     <Content Include="$(PlaceholderFile)" PackagePath="lib/netstandard1.0" />
   </ItemGroup>
 
@@ -58,12 +57,12 @@
   
   <UsingTask TaskName="GenerateRuntimeGraph" AssemblyFile="$(_generateRuntimeGraphTask)"/>
 
-  <Target Name="GenerateRuntimeJson" Condition="'$(AdditionalRuntimeIdentifiers)' != ''">
+  <Target Name="GenerateRuntimeJson" AfterTargets="Build" Condition="'$(AdditionalRuntimeIdentifiers)' != ''">
     <MakeDir Directories="$(IntermediateOutputPath)" />
     <GenerateRuntimeGraph RuntimeGroups="@(RuntimeGroupWithQualifiers)"
                           AdditionalRuntimeIdentifiers="$(AdditionalRuntimeIdentifiers)"
                           AdditionalRuntimeIdentifierParent="$(AdditionalRuntimeIdentifierParent)"
-                          RuntimeJson="$(IntermediateOutputPath)runtime.json"
+                          RuntimeJson="$(BaseOutputPath)runtime.json"
                           UpdateRuntimeFiles="True" />
   </Target>
 
diff --git a/src/runtime/src/libraries/oob-all.proj b/src/runtime/src/libraries/oob-all.proj
index f84c5b4050251..9164a5846ceb4 100644
--- a/src/runtime/src/libraries/oob-all.proj
+++ b/src/runtime/src/libraries/oob-all.proj
@@ -16,8 +16,7 @@
     <!-- Build these packages in the allconfigurations leg only. -->
     <ProjectReference Remove="Microsoft.Internal.Runtime.AspNetCore.Transport\src\Microsoft.Internal.Runtime.AspNetCore.Transport.proj;
                               Microsoft.Internal.Runtime.WindowsDesktop.Transport\src\Microsoft.Internal.Runtime.WindowsDesktop.Transport.proj;
-                              Microsoft.Windows.Compatibility\src\Microsoft.Windows.Compatibility.csproj;
-                              Microsoft.NETCore.Platforms\src\Microsoft.NETCore.Platforms.csproj"
+                              Microsoft.Windows.Compatibility\src\Microsoft.Windows.Compatibility.csproj"
                       Condition="'$(BuildAllConfigurations)' != 'true'" />
 
     <!-- Skip these projects during source-build as they rely on external prebuilts. -->
diff --git a/src/runtime/src/libraries/oob-src.proj b/src/runtime/src/libraries/oob-src.proj
index 2cbfdf9bb3c55..472a2dfb5e708 100644
--- a/src/runtime/src/libraries/oob-src.proj
+++ b/src/runtime/src/libraries/oob-src.proj
@@ -21,8 +21,7 @@
                                                                                                             ('$(BuildAllConfigurations)' != 'true' and '$(RuntimeFlavor)' == '$(PrimaryRuntimeFlavor)')" />
 
     <!-- Don't build task and tools project in the NetCoreAppCurrent vertical. -->
-    <ProjectReference Remove="Microsoft.NETCore.Platforms\src\Microsoft.NETCore.Platforms.csproj;
-                              Microsoft.XmlSerializer.Generator\src\Microsoft.XmlSerializer.Generator.csproj" />
+    <ProjectReference Remove="Microsoft.XmlSerializer.Generator\src\Microsoft.XmlSerializer.Generator.csproj" />
 
     <!-- Don't build meta-projects in the NetCoreAppCurrent vertical. -->
     <ProjectReference Remove="Microsoft.Internal.Runtime.AspNetCore.Transport\src\Microsoft.Internal.Runtime.AspNetCore.Transport.proj;
diff --git a/src/runtime/src/libraries/pretest.proj b/src/runtime/src/libraries/pretest.proj
index b6e9721ca79a8..ac4f3c9021c0c 100644
--- a/src/runtime/src/libraries/pretest.proj
+++ b/src/runtime/src/libraries/pretest.proj
@@ -94,7 +94,7 @@
           Condition="'$(BuildTargetFramework)' == '$(NetCoreAppCurrent)' or '$(BuildTargetFramework)' == ''">
     <!-- Shared framework deps file generation. Produces a test shared-framework deps file. -->
     <GenerateTestSharedFrameworkDepsFile SharedFrameworkDirectory="$(NetCoreAppCurrentTestHostSharedFrameworkPath)"
-                                         RuntimeGraphFiles="$(RuntimeIdGraphDefinitionFile)"
+                                         RuntimeGraphFiles="$(BundledRuntimeIdentifierGraphFile)"
                                          TargetRuntimeIdentifier="$(PackageRID)" />
   </Target>
 
