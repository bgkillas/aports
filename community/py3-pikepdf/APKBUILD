# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=py3-pikepdf
_pyname=pikepdf
pkgver=6.2.4
pkgrel=0
pkgdesc="Python library for reading and writing PDF"
url="https://github.com/pikepdf/pikepdf"
arch="all"
license="MPL-2.0"
depends="
	py3-deprecation
	py3-lxml
	py3-packaging
	py3-pillow
	python3
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-pybind11-dev
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	python3-dev
	qpdf-dev
	"
checkdepends="
	py3-hypothesis
	py3-psutil
	py3-pytest
	py3-pytest-xdist
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/pikepdf/pikepdf/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver

# secfixes:
#   2.9.1-r2:
#     - CVE-2021-29421

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python -m installer -d test_install \
		dist/pikepdf-*.whl
	PYTHONPATH="$(echo $PWD/test_install/usr/lib/python3*/site-packages)" pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/pikepdf-*.whl
}

sha512sums="
f8316449a9f0ba2b42d0d0fa4e353a131902c074c35dbe26b55946df4ad0c83cdea4b91f7d1dab15287dc08bcce438a5a232950dba3cfee369093706c8cbbc25  py3-pikepdf-6.2.4.tar.gz
"
