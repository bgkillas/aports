# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libsigc++
pkgver=2.10.8
pkgrel=0
pkgdesc="type-safe Signal Framework for C++"
url="https://github.com/libsigcplusplus/libsigcplusplus"
arch="all"
license="LGPL-3.0-or-later"
makedepends="meson docbook-xsl perl doxygen graphviz libxslt"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.gnome.org/sources/libsigc++/${pkgver%.*}/libsigc++-$pkgver.tar.xz"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dbuild-documentation=true \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

doc() {
	default_doc

	mv "$pkgdir"/usr/share/devhelp "$subpkgdir"/usr/share
	rmdir "$pkgdir"/usr/share
}

sha512sums="
8b22fd8ae4eca3ebc1b65b68d4dc022e7bbde6d1d02a557e64d2fda2682e2e39b357af6d8b68e8741c287701be9fffd27125c6171790455a7657e0ea55cc08b3  libsigc++-2.10.8.tar.xz
"
