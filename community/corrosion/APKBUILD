# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=corrosion
pkgver=0.3.0
pkgrel=0
pkgdesc="Marrying Rust and CMake - Easy Rust and C/C++ Integration!"
url="https://github.com/corrosion-rs/corrosion"
# riscv64: rust broken
arch="all !riscv64"
license="MIT"
depends="
	cargo
	cmake
	rust
	"
makedepends="samurai"
source="https://github.com/AndrewGaspar/corrosion/archive/v$pkgver/corrosion-$pkgver.tar.gz"
options="net" # Required to download Rust dependencies

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True
	cmake --build build
}

check() {
	cd build

	# parse_target_triple_build is broken, expects rustup and fails without it
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "parse_target_triple_build"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e67ca52e3012ca8669786ce4e03f305a69367df55605c08797c01b061844edf2aa8753b734aeb163dc2ccbea13522e143d0cffb0144f6cbab39e65d8a734dd4b  corrosion-0.3.0.tar.gz
"
