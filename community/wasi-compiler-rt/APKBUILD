# Contributor: psykose <alice@ayaya.dev>
# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=wasi-compiler-rt
# match llvm ver
pkgver=15.0.5
_llvmver="${pkgver%%.*}"
_wasi_sdk_ver=16
pkgrel=0
pkgdesc="WASI LLVM compiler runtime"
url="https://compiler-rt.llvm.org/"
arch="all"
options="!check" # TODO: check
license="Apache-2.0 WITH LLVM-exception BSD-2-Clause"
makedepends="
	clang
	cmake
	libxml2-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	python3-dev
	samurai
	wasi-libc
	wasi-libcxx
	zlib-dev
	"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/llvm-project-$pkgver.src.tar.xz
	https://github.com/WebAssembly/wasi-sdk/archive/refs/tags/wasi-sdk-$_wasi_sdk_ver.tar.gz
	"
builddir="$srcdir"/llvm-project-$pkgver.src

prepare() {
	default_prepare

	mv "$srcdir"/wasi-sdk-wasi-sdk-$_wasi_sdk_ver/wasi-sdk.cmake "$builddir"
	mv "$srcdir"/wasi-sdk-wasi-sdk-$_wasi_sdk_ver/cmake/Platform cmake
}

build() {
	export CFLAGS="$CFLAGS -fno-exceptions --sysroot=/usr/share/wasi-sysroot"
	cmake -B build -G Ninja -S compiler-rt -Wno-dev \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_MODULE_PATH="$builddir"/cmake \
		-DCMAKE_TOOLCHAIN_FILE="$builddir"/wasi-sdk.cmake \
		-DCMAKE_C_COMPILER_WORKS=ON \
		-DCMAKE_CXX_COMPILER_WORKS=ON \
		-DCOMPILER_RT_BAREMETAL_BUILD=ON \
		-DCOMPILER_RT_INCLUDE_TESTS=OFF \
		-DCOMPILER_RT_HAS_FPIC_FLAG=OFF \
		-DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON \
		-DCOMPILER_RT_OS_DIR=wasi \
		-DWASI_SDK_PREFIX=/usr \
		-DCMAKE_INSTALL_PREFIX=/usr/lib/clang/$pkgver/
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	rm -r "$pkgdir"/usr/lib/clang/$pkgver/include
}

sha512sums="
cf2a89ebb6bc9d7e3f1fd09531c84ac9927cdbe5ee13a6fcb8ce4d08e5a9d6d480ad982f62126b9c757beafa283b34f2ba1fbc56223c641e70da2be4627f59a6  llvm-project-15.0.5.src.tar.xz
501467cb04ee85ab2ccc3d8ab1beb5dd8957ca71cc51c86fd357991ddccb1a8c2656e24b947ea3a5acfaafd8c762f5ba20458c22b58a5a5c85ef8ecb7a76db65  wasi-sdk-16.tar.gz
"
