# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=certbot
pkgver=1.32.0
pkgrel=0
pkgdesc="An ACME client that can update Apache/Nginx configurations"
url="https://github.com/certbot/certbot"
arch="noarch"
license="Apache-2.0"
depends="
	py3-acme
	py3-configargparse
	py3-configobj
	py3-cryptography
	py3-distro
	py3-distutils-extra
	py3-josepy
	py3-parsedatetime
	py3-pyrfc3339
	py3-tz
	py3-setuptools
	py3-zope-component
	py3-zope-interface
"
checkdepends="py3-augeas py3-pytest"
replaces="letsencrypt"
source="https://pypi.io/packages/source/c/certbot/certbot-$pkgver.tar.gz"

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
128825c89558e31e7ad5f5271397fb12ab55c371f3c614ca4df4d23b28f0b9e18775670c3ace29416a4d5f76acde0881b0033743c1446103235058ad8a96b07d  certbot-1.32.0.tar.gz
"
