# Contributor: Stuart Cardall <developer@it-offshore.co.uk>
# Maintainer: Stuart Cardall <developer@it-offshore.co.uk>
pkgname=xpra
pkgver=4.4.2
pkgrel=1
pkgdesc="Xpra is 'screen for X' & allows you to run X programs, usually on a remote host over SSH or encrypted tcp"
url="https://xpra.org"
arch="all"
license="GPL-2.0-or-later"
depends="
	py3-gobject3
	py3-numpy
	py3-opengl
	py3-rencode
	py3-lz4
	py3-cryptography
	py3-dbus
	py3-openssl
	py3-brotli
	py3-uinput
	py3-inotify

	py3-pillow

	xf86-video-dummy
	xorg-server
	xvfb
	"
makedepends="
	x264-dev
	x265-dev
	ffmpeg-dev
	libvpx-dev
	libwebp-dev
	libpng-dev
	libjpeg-turbo-dev

	libogg-dev
	opus-dev
	libvorbis-dev

	libx11-dev
	libxcomposite-dev
	libxdamage-dev
	libxfixes-dev
	libxkbfile-dev
	libxrandr-dev
	libxres-dev
	libxtst-dev

	gtk+3.0-dev
	glib-dev

	py3-gobject3-dev
	py3-cairo-dev

	lz4-dev

	python3-dev
	cython
	yasm
	nasm
	gmp-dev
	mpfr-dev
	"
subpackages="$pkgname-openrc $pkgname-doc $pkgname-tests::noarch"
options="!check"
source="https://xpra.org/src/xpra-$pkgver.tar.xz
	$pkgname-fix-32bit-overflow.patch::https://github.com/Xpra-org/xpra/commit/3f5eb97283e73ca78059be5dd828a87b393a38c7.patch
	"

build() {
	python3 setup.py build \
		--with-bundle_tests
}

package() {
	python3 setup.py install --without-docs --prefix=/usr --root="$pkgdir"

	# Fixes Error relocating /usr/lib/xorg/modules/drivers/dummy_drv.so: fbPictureInit: symbol not found
	# https://bugs.alpinelinux.org/issues/5478
	printf '\nSection "Module"\n  Load\t"fb"\nEndSection\n' >> "$pkgdir"/etc/xpra/xorg.conf

	rm -rf "$pkgdir"/usr/lib/tmpfiles.d/xpra.conf
	rm -rf "$pkgdir"/usr/lib/sysusers.d/xpra.conf

	# Fix location of dbus conf
	mkdir -p "$pkgdir"/usr/share/dbus-1
	mv "$pkgdir"/etc/dbus-1/system.d "$pkgdir"/usr/share/dbus-1
}

tests() {
	pkgdesc="Xpra test suite"

	mkdir -p "$subpkgdir"/usr/share/xpra
	cp -rf "$builddir"/tests "$subpkgdir"/usr/share/xpra/
}

sha512sums="
f746b02a2c326cf7e992b05b0c989f83b92ad1d527f564c2d9852823ff87b0585ea43a675dd590605003043e73aac1dbbae20bd37ed05b6c02de58e0829c18a0  xpra-4.4.2.tar.xz
8a01c648ca9c3f5d02b0cdbd4ccf9e83ff2e1f32e64ec7715f5688d57f9410dee9f5f03f37e6e746d5ce3005e26b7c10c8904bd1a873804aa7465f2cd3442524  xpra-fix-32bit-overflow.patch
"
