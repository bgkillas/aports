# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtmultimedia
pkgver=6.4.1
pkgrel=1
pkgdesc="Classes for audio, video, radio and camera functionality"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	alsa-lib-dev
	ffmpeg-dev
	gst-plugins-base-dev
	gstreamer-dev
	libva-glx-dev
	libxv-dev
	pulseaudio-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtshadertools-dev
	qt6-qtsvg-dev
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
builddir="$srcdir/qtmultimedia-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtmultimedia-everywhere-src-${pkgver/_/-}.tar.xz
	select.patch
	"

build() {
	case "$CARCH" in
	x86)
		# new qtmultimedia requires -msse since 6.4 (intrinsic use). our baseline
		# is now sse2 anyway, so this is ok
		export CFLAGS="$CFLAGS -msse"
		export CXXFLAGS="$CXXFLAGS -msse"
		;;
	*)
	esac
	# alsa disabled due to build failure for now
	# src/multimedia/platform/alsa/qalsaaudiosink.cpp:270:5: error: 'timeStamp' was not declared in this scope
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DINSTALL_BINDIR=lib/qt6/bin \
		-DINSTALL_DOCDIR=share/doc/qt6 \
		-DINSTALL_ARCHDATADIR=lib/qt6 \
		-DINSTALL_DATADIR=share/qt6 \
		-DINSTALL_INCLUDEDIR=include/qt6 \
		-DINSTALL_MKSPECSDIR=lib/qt6/mkspecs \
		-DINSTALL_EXAMPLESDIR=share/doc/qt6/examples \
		-DQT_FEATURE_alsa=OFF \
		-DQT_FEATURE_pulseaudio=ON
	cmake --build build --parallel
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
f221bedaf997b413e1fa8a984503b03839ac731c99e9ab4233871acde57b0fd66b64ec401448505b25e6c1a5313b594a01b65ba9af0047ed008a2fea8e40d6c9  qtmultimedia-everywhere-src-6.4.1.tar.xz
5c0f5a50952544d183c16f8b4b4f490bebb0260f255fed7bcd853081585792457792d72d5204307dae3133ca395044a90a14edab205d0ae28e0e3832ff995326  select.patch
"
