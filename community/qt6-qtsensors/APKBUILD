# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtsensors
pkgver=6.4.1
pkgrel=0
pkgdesc="Provides access to sensor hardware and motion gesture recognition"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtsvg-dev
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
builddir="$srcdir/qtsensors-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtsensors-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DINSTALL_BINDIR=lib/qt6/bin \
		-DINSTALL_DOCDIR=share/doc/qt6 \
		-DINSTALL_ARCHDATADIR=lib/qt6 \
		-DINSTALL_DATADIR=share/qt6 \
		-DINSTALL_INCLUDEDIR=include/qt6 \
		-DINSTALL_MKSPECSDIR=lib/qt6/mkspecs \
		-DINSTALL_EXAMPLESDIR=share/doc/qt6/examples
	cmake --build build --parallel
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
14242fd6bed762cdb74f5de4736d3238ec3bebafec30502e1096388e7c0c0f88e5881e3b3cab30ddd304c693ecf076b307fd5e1f75cd67c4898eca1eab71339d  qtsensors-everywhere-src-6.4.1.tar.xz
"
