# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=feedbackd
pkgver=0.0.1
pkgrel=0
pkgdesc="A daemon to provide haptic (and later more) feedback on events"
url="https://source.puri.sm/Librem5/feedbackd"
arch="all"
license="GPL-3.0-or-later"
depends="dbus feedbackd-device-themes"
makedepends="meson glib-dev gsound-dev libgudev-dev json-glib-dev gtk-doc
	vala gobject-introspection-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://source.puri.sm/Librem5/feedbackd/-/archive/v$pkgver/feedbackd-v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-v$pkgver"
install="$pkgname.pre-install $pkgname.pre-upgrade"

build() {
	abuild-meson \
		-Dgtk_doc=true \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
	install -Dm644 "$builddir"/debian/feedbackd.udev \
		"$pkgdir"/usr/lib/udev/rules.d/90-feedbackd.rules
}

sha512sums="
903e597d3883dc49c06a3032fdd398f98ea5ea68ed54d5bd79bcf471abe89982ca2559558b98658eca193bc7c1ca505955d29b3a054fc5e2a2c7f5cbeabcc78b  feedbackd-v0.0.1.tar.gz
"
