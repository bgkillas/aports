# Maintainer: psykose <alice@ayaya.dev>
pkgname=limine
pkgver=4.20221119.0
pkgrel=0
pkgdesc="Advanced multiprotocol x86/x86_64 BIOS/UEFI Bootloader"
url="https://limine-bootloader.org"
# only aarch64/x86_64 supported, x86 is meh
arch="aarch64 x86_64"
license="BSD-2-Clause"
makedepends="
	clang
	coreutils
	lld
	llvm
	mtools
	nasm
	"
subpackages="
	$pkgname-dev
	$pkgname-cd:_cd
	$pkgname-deploy
	$pkgname-pxe
	$pkgname-sys
	$pkgname-32:_32
	$pkgname-64:_64
	$pkgname-aarch64:_64_arm
	"
source="https://github.com/limine-bootloader/limine/releases/download/v$pkgver/limine-$pkgver.tar.xz"
options="!check" # no tests in tarball

build() {
	./configure \
		--host=$CHOST \
		--prefix=/usr \
		--enable-all
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

deploy() {
	pkgdesc="$pkgdesc (limine-deploy bios installer)"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-sys=$pkgver-r$pkgrel"

	amove usr/bin/limine-deploy
}

_cd() {
	pkgdesc="$pkgdesc (cd/efi files)"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-sys=$pkgver-r$pkgrel"

	amove usr/share/limine/limine-cd*.bin
}

pxe() {
	pkgdesc="$pkgdesc (pxe executable)"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-sys=$pkgver-r$pkgrel"

	amove usr/share/limine/limine-pxe.bin
}

sys() {
	pkgdesc="$pkgdesc (sys file)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/limine.sys
}

_32() {
	pkgdesc="$pkgdesc (32-bit uefi image)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/BOOTIA32.EFI
}

_64() {
	pkgdesc="$pkgdesc (64-bit uefi image)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/BOOTX64.EFI
}

_64_arm() {
	pkgdesc="$pkgdesc (ARM 64-bit uefi image)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/BOOTAA64.EFI
}

sha512sums="
fa092d7ddab17c86c12b5bf56bd397f7c7f5d77302677e1a822321651460fe15509869907aef69649a38284ed8d06e8c11275560fba75a05a360bc97c70018ed  limine-4.20221119.0.tar.xz
"
