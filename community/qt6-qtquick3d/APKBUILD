# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtquick3d
pkgver=6.4.1
pkgrel=0
pkgdesc="Qt module and API for defining 3D content in Qt Quick"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtdeclarative-dev
	qt6-qtquicktimeline-dev
	qt6-qtshadertools-dev
	vulkan-headers
	"
makedepends="$depends_dev
	assimp-dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtquick3d-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtquick3d-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c62226eb1744c0213a901fcf4b56cb3cfa27c5b25881b8f068dbf8e0ed0846e7775e768fe97fe0dfe1ce3d3d4815700b2f3d75d6626d2ac3db1a6328995e454d  qtquick3d-everywhere-src-6.4.1.tar.xz
"
