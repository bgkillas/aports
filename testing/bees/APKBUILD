# Contributor: Nicolas Lorin <androw95220@gmail.com>
# Maintainer: Bradley Saulteaux <bradsoto@gmail.com>
pkgname=bees
pkgver=0.7.2
pkgrel=0
pkgdesc="Best-Effort Extent-Same, a btrfs dedup agent"
url="https://github.com/Zygo/bees"
arch="all"
license="GPL-3.0-or-later"
depends="btrfs-progs"
makedepends="btrfs-progs-dev util-linux-dev"
subpackages="$pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Zygo/bees/archive/v$pkgver.tar.gz
	no-werror.patch
	10-pthread_getname1.patch
	ppc64le_fix_min_compare.patch
	bees.initd
	"

[ "$CARCH" = "riscv64" ] && options="$options textrels"

build() {
	make all
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	mv bin "$pkgdir"/
	install -m755 -D "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/$pkgname
}

sha512sums="
0a07a83d025c077305bbfdafd4ae62c86f6857c6e90091215e3640e38dd365d4ee384af6f25eb186824e40bb18c3ac6bf6abb3962d62c9ba25e7799137ca1fce  bees-0.7.2.tar.gz
1e9de7b991db9614993d734e63649d8d5803b02423bba41370fdcc7094e6384017fe8a3b5d8f3b3beb86ca5dab33c2b05b0acf77e7eb0825944e176d10c5b15b  no-werror.patch
877a90b0be60357ba4778f30a89003270b93af8cc5995e2df3fb58147733a39e972129f693ce9239e8987c589f3c43384774c3baecd042e88abdb2b07d1ce2ef  10-pthread_getname1.patch
2e13a670184d71b64e04450adb182d0a6e842e1a7d561882e0a07ae4ea7a9ed15a3fcce03f61c5412eabdb2fadf559f221e21ffd10440c4b8b700f3eab02aab4  ppc64le_fix_min_compare.patch
093bc4c9604a0b28b39069e447d83800c91d0974fe4618ce5e5063e5c816b2d63c1b633710c592d76e8f6367d696283d6fa4a3a9561b09ce62fa28cabf8e55d0  bees.initd
"
