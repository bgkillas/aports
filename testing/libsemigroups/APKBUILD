# Contributor: Grigory Kirillov <txgk@bk.ru>
# Maintainer: Grigory Kirillov <txgk@bk.ru>
pkgname=libsemigroups
pkgver=2.3.2
pkgrel=0
pkgdesc="Library for computing semigroups and monoids"
url="https://github.com/libsemigroups/libsemigroups"
arch="all"
license="GPL-3.0-or-later"
makedepends="eigen-dev fmt-dev"
subpackages="$pkgname-static $pkgname-dev"
source="https://github.com/libsemigroups/libsemigroups/releases/download/v$pkgver/libsemigroups-$pkgver.tar.gz"

build() {
	export CXXFLAGS="${CXXFLAGS/-Os/-O3}" # gotta go fast

	./configure \
		--target="$CTARGET" \
		--build="$CBUILD" \
		--host="$CHOST" \
		--prefix=/usr \
		--enable-eigen \
		--enable-fmt \
		--with-external-eigen \
		--with-external-fmt
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
13b0c2ca92d64e9efc798ca49e9446416c2b50e770212a4e695810bdd1fdfc3b72b2f017fb25a09611e45eb71151c83751ab325d3369512393d24b795ca227c8  libsemigroups-2.3.2.tar.gz
"
