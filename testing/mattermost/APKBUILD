# Contributor: Lauren N. Liberda <lauren@selfisekai.rocks>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=mattermost
pkgver=5.2.1
pkgrel=1
pkgdesc="Open source collaboration for developers"
url="https://mattermost.com/"
arch="aarch64 x86_64"	# electron
license="Apache-2.0"
depends="electron"
makedepends="
	electron-dev
	electron-tasje
	nodejs
	npm
	python3
	"
options="!check"	# broken
checkdepends="libx11-dev make"
source="
	https://github.com/mattermost/desktop/archive/refs/tags/v$pkgver/mattermost-v$pkgver.tar.gz

	tasje-fix-icons.patch
	tasje-fix-modules.patch
	build-size.patch

	mattermost
	"
builddir="$srcdir/desktop-$pkgver"

prepare() {
	default_prepare

	git init
	git commit --allow-empty --message "$(date -Is)" --no-gpg-sign

	msg "Downloading dependencies"
	npm ci --ignore-scripts
}

build() {
	msg "Building the app resources"
	NODE_ENV=production npm run build

	msg "Building native modules"
	(
		# it doesn't have an install script with node-gyp and doesn't even have it in deps
		cd node_modules/macos-notification-state
		../.bin/node-gyp rebuild --build-from-source --nodedir=/usr/include/electron/node_headers
	)

	msg "Removing devDependencies"
	npm prune --omit=dev

	msg "Packaging the app"
	tasje pack --config electron-builder.json
}

check() {
	export ELECTRON_OVERRIDE_DIST_PATH=/usr/bin/electron
	npm rebuild robotjs --build-from-source --nodedir=/usr/include/electron/node_headers

	npm run test
}

package() {
	install -Dm644 release/resources/app.asar "$pkgdir"/usr/lib/$pkgname/app.asar
	install -Dm644 release/mattermost-desktop.desktop "$pkgdir"/usr/share/applications/$pkgname.desktop
	install -Dm755 "$srcdir"/mattermost "$pkgdir"/usr/bin/mattermost-desktop

	while read -r size; do
		install -Dm644 release/icons/$size.png "$pkgdir"/usr/share/icons/hicolor/$size/apps/$pkgname.png
	done < release/icons/size-list
}

sha512sums="
33bf073556682bc3b270b97e0bca341dd061690592210c6cd80a385f17709d97a460fe31a6bc7c2bed3c4ce49bf9449fec1dfe04a550699d3dbfc0406d1f363c  mattermost-v5.2.1.tar.gz
2722b35d827d38841fc3f02698434b93f05c1ea11bd67f25c9bd8076ff6716c6e250ff7ba599a4666a521b5e66f40da4e418f4e7de7cb5b40fcd68ffc9686b3e  tasje-fix-icons.patch
24e8088b585937fe0cd7019a33c6d4ff006f31b732897d3f3295bcef12b49a81c10f43bc862c510413b7624a2b901b6c5a1ca065d8c2dd46626c2a403ef8fe25  tasje-fix-modules.patch
39f040cebbf18e0076fdb41497e0b40337d0d7e7364d7037f72818098487a22d57137b12aee9a92da501d9af52c4895bf6f2f1d8c7c3936486e50a9353fe1d0d  build-size.patch
b88e67b980d7fa30be46bc33279856b393cf779badf941e3d38a793e569dfec21b4d68f0528150ad51fe534eddff8ee0058d86d13a6dd15f46256004d577313a  mattermost
"
